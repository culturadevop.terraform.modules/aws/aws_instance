variable "resources" {
  description = "resources of objects to create"
  nullable    = false
  type = list(object({
    name                        = string
    instance_type               = string
    ami                         = string
    user_data_replace_on_change = optional(bool)
    user_data_base64            = optional(string)
    subnet_id                   = optional(string)
    associate_public_ip_address = optional(bool)
    tags                        = optional(map(string))
  }))
}

variable "project" {
  description = "some common settings like project name, environment, and tags for all resources-objects"
  nullable    = false
  type = object({
    project     = string
    environment = string
    createdBy   = string
    group       = string
  })
}


